# Approximate nearest neighbor Assignment


Nearest neighbor search (NNS), as a form of proximity search, is the optimization problem of finding the point in a given set that is closest (or most similar) to a given point. Closeness is typically expressed in terms of a dissimilarity function: the less similar the objects, the larger the function values.

#### Dataset Used for Assignment - https://www.kaggle.com/c/data-science-for-good-careervillage
#### Data Prepration Google Colab Link - https://colab.research.google.com/drive/1ySmpIKAZURwfAS32AFj747wcyvnPOw44?usp=sharing
#### Google Colab Link - https://colab.research.google.com/drive/1-6vph2QGHPO2pb8ZtRhXMMqcifTNx6aS?usp=sharing

### Libraries Used for implementation- 
1. faiss
2. nmslib
3. annoy


### Results- 

#### Locality Sensitive Hashing 
Using Locality Sensitive Hashing the most similar questions to -> what kind of  college could i go  to for a soccer player are:
* How can one create an original voice in applying for college?

* what kind of  college could i go  to for a soccer player

* What can one do with a degree in religion or religious studies?

* Which would be the uncomplicated class that a student is required to take while majoring in the education field with a bachelor's degree?

* What are some great programs to watch in Spanish to help improve my fluency?

* What is the difference between art school, College, and Community college?

* What kind of jobs would people with a doctorate in linguistics get?

LSH took 0.032872915267944336 seconds.

#### Exhaustive Search
Using Exhaustive Search the most similar questions to -> what kind of  college could i go  to for a soccer player are:
* what kind of  college could i go  to for a soccer player

* How do I prevent the temptation to go out every night while in college?

* Do I need a car in college?

* How does a division III soccer program differ from a division I program?

* What can you do with a degree in Women and Gender Studies?

* Is it more affordable to live in residence or not?

* How difficult or easy is the transition from High School to College, especially if you're going out of state?

Exhaustive Search took 0.035326242446899414 seconds.

### Trees and Graph Algorithms
Using Trees and Graph Algorithms the most similar questions to -> what kind of  college could i go  to for a soccer player are:
* what kind of  college could i go  to for a soccer player

* How do I prevent the temptation to go out every night while in college?

* Do I need a car in college?

* How does a division III soccer program differ from a division I program?

* What can you do with a degree in Women and Gender Studies?

* Is it more affordable to live in residence or not?

* How difficult or easy is the transition from High School to College, especially if you're going out of state?

Trees and Graph Algorithms took 0.4027230739593506 seconds.


### Product Quantization
Using Product Quantization the most similar questions to -> what kind of  college could i go  to for a soccer player are:
* what kind of  college could i go  to for a soccer player

* How do I prevent the temptation to go out every night while in college?

* Do I need a car in college?

* How does a division III soccer program differ from a division I program?

* What can you do with a degree in Women and Gender Studies?

* Is it more affordable to live in residence or not?

* How difficult or easy is the transition from High School to College, especially if you're going out of state?

Product Quantization took 3.3029775619506836 seconds.

### Hierarchical Navigable Small World Graphs
Using Hierarchical Navigable Small World Graphs the most similar questions to -> what kind of  college could i go  to for a soccer player are:
* what kind of  college could i go  to for a soccer player

* Is it really worth it to go to a big name school?

* What is a good university to attend a good soccer team ?

* Do I need a car in college?

* What are some good ways to earn scholarships

* whats the limit of times you should change your major and why?

* How whats one major thing to worry about going into college?

Hierarchical Navigable Small World Graphs took 11.669643878936768 seconds.